# Módulo Street Lines

Versão 1.2 testada no Magento 2.4 com o checkout default do Magento

Esse módulo é responsável por adicionar as Labels, "Rua", "Número", "Complemento" e "Bairro', nos campos de endereço no checkout, e nas páginas
que utilizam endereço.
Para que funcione corretamente, primeiro é necessário verificar se está sendo utilizado 4 linhas no attributo Rua. Para fazer essa verificação, acesse:
Lojas -> Atributos -> Endereço do Cliente. Na tela que surgir selecione o atributo street. Na tela de edião do atributo verifique se o campo
"Contagem de linhas" está setado com o valor "4".
